// Usamos un diccionario para evitar 'strings mágicos'
export const ENV_VARS = Object.freeze({
  USERNAME: 'USERNAME',
  PASSWORD: 'PASSWORD',
});
