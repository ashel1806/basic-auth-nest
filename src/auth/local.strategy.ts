import { BasicStrategy as Strategy } from 'passport-http';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from '@nestjs/config';
import { ENV_VARS } from 'src/common/constans';

@Injectable()
export class BasicStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly configService: ConfigService) {
    super({
      passReqToCallback: true,
    });
  }

  // Función que validará los datos que se pasan
  async validate(req, username: string, password: string): Promise<any> {
    if (
      this.configService.get<string>(ENV_VARS.USERNAME) === username &&
      this.configService.get<string>(ENV_VARS.PASSWORD) === password
    ) {
      return true;
    }

    // Si los valores no son correctos, se envía una excepción
    throw new UnauthorizedException();
  }
}
